﻿using BSA.BL.DTO.Project;
using BSA.BL.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA.BL.DTO.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public Repo.Enums.TaskState State { get; set; }

        public int ProjectId { get; set; }
        public ProjectDTO Project { get; set; }

        public int PerformerId { get; set; }
        public UserDTO Performer { get; set; }
    }
}
