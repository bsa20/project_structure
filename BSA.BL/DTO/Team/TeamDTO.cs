﻿using System;

namespace BSA.BL.DTO.Team
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
