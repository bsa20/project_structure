﻿using BSA.BL.DTO.Project;
using BSA.BL.DTO.Task;

namespace BSA.BL.DTO.User
{
    public class UserMainInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int NumberOfTasksOfLastProject { get; set; }
        public int NumberOfNotFinishedTasks { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
