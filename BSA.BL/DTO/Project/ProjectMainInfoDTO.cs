﻿using BSA.BL.DTO.Task;

namespace BSA.BL.DTO.Project
{
    public class ProjectMainInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortetTaskByName { get; set; }
        public int NumberOfUsersInTeam { get; set; }
    }
}
