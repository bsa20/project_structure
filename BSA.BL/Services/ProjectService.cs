﻿using AutoMapper;
using BSA.BL.DTO.Project;
using BSA.BL.DTO.Task;
using BSA.BL.Exceptions;
using BSA.Repo.Models;
using BSA.Repo.UnitOfWorker;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.WebAPI.Services
{
    public class ProjectService
    {
        private readonly UnitOfWorker _uow;
        private readonly IMapper _mapper;

        public ProjectService(UnitOfWorker uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(_uow.Projects.GetAll());
        }

        public ProjectDTO GetProjectById(int id)
        {
            var proj = _uow.Projects.GetById(id);
            if(proj == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }

            return _mapper.Map<ProjectDTO>(proj);
        }

        public ProjectDTO AddProject(NewProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);

            var team = _uow.Teams.GetById(project.TeamId);
            if (team == null) throw new NotFoundException(nameof(Team), project.TeamId);
            var user = _uow.Users.GetById(project.AuthorId);
            if (user == null) throw new NotFoundException(nameof(User), project.AuthorId);

            _uow.Projects.Add(projectEntity);

            _uow.Save();

            var proj = _uow.Projects.GetById(projectEntity.Id);
            return _mapper.Map<ProjectDTO>(proj);
        }

        public void UpdateProject(NewProjectDTO project, int id)
        {
            var proj = _uow.Projects.GetById(id);
            if (proj == null)throw new NotFoundException(nameof(Project), id);
            var team = _uow.Teams.GetById(project.TeamId);
            if (team == null) throw new NotFoundException(nameof(Team), project.TeamId);
            var user = _uow.Users.GetById(project.AuthorId);
            if (user == null) throw new NotFoundException(nameof(User), project.AuthorId);

            var projectEntity = _mapper.Map<Project>(project);
            projectEntity.Id = id;
            projectEntity.CreatedAt = proj.CreatedAt;

            _uow.Projects.Update(projectEntity);
            _uow.Save();
        }

        public void DeleteProject(int id)
        {
            var proj = _uow.Projects.GetById(id);
            if (proj == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }

            _uow.Projects.Remove(id);
            _uow.Save();
        }

        // 7
        public IEnumerable<ProjectMainInfoDTO> GetMainInfoAboutProjects()
        {
            var projects = _uow.Projects.GetAll();
            var users = _uow.Users.GetAll();

            return projects.Select(p => new ProjectMainInfoDTO
            {
                Project = _mapper.Map<ProjectDTO>(p),
                LongestTaskByDescription = _mapper.Map<TaskDTO>(p.Tasks.Count() == 0 ? null : p.Tasks.Aggregate((max, t) => (t.Description.Length > max.Description.Length ? t : max))),
                ShortetTaskByName = _mapper.Map<TaskDTO>(p.Tasks.Count() == 0 ? null : p.Tasks.Aggregate((min, t) => (t.Name.Length < min.Name.Length ? t : min))),
                NumberOfUsersInTeam = (p.Description.Length > 20 || p.Tasks.Count() < 3) ? users.Count(u => u.TeamId == p.TeamId) : 0
            }).ToList();
        }
    }
}
