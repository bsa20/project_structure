﻿using AutoMapper;
using BSA.BL.DTO.Team;
using BSA.BL.DTO.User;
using BSA.BL.Exceptions;
using BSA.Repo.Models;
using BSA.Repo.UnitOfWorker;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BL.Services
{
    public class TeamService
    {
        private readonly UnitOfWorker _uow;
        private readonly IMapper _mapper;

        public TeamService(UnitOfWorker uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public IEnumerable<TeamDTO> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(_uow.Teams.GetAll());
        }

        public TeamDTO GetTeamById(int id)
        {
            var team = _uow.Teams.GetById(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            return _mapper.Map<TeamDTO>(team);
        }

        public TeamDTO AddTeam(NewTeamDTO team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            _uow.Teams.Add(teamEntity);

            _uow.Save();

            var newTeam = _uow.Teams.GetById(teamEntity.Id);
            return _mapper.Map<TeamDTO>(newTeam);
        }

        public void UpdateTeam(NewTeamDTO team, int id)
        {
            var teamCheck = _uow.Teams.GetById(id);
            if (teamCheck == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            var teamEntity = _mapper.Map<Team>(team);
            teamEntity.Id = id;
            teamEntity.CreatedAt = teamCheck.CreatedAt;

            _uow.Teams.Update(teamEntity);
            _uow.Save();
        }

        public void DeleteTeam(int id)
        {
            var team = _uow.Teams.GetById(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            _uow.Teams.Remove(id);
            _uow.Save();
        }

        // 4
        public IEnumerable<TeamWithUsersDTO> GetTeamsWithUsersOlderThanTenYears()
        {
            var teams = _uow.Teams.GetAll();
            var users = _uow.Users.GetAll();

            return teams.GroupJoin(users.Where(u => CalculateAge(u.Birthday) > 10), t => t.Id, u => u.TeamId, (t, u) => new TeamWithUsersDTO
            {
                Id = t.Id,
                Name = t.Name,
                Users = _mapper.Map<IEnumerable<UserDTO>>(u.OrderByDescending(u => u.RegisteredAt).ToList())
            }).Where(t => t.Users.Any()).ToList();
        }

        private int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }
    }
}
