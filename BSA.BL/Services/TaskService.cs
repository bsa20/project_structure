﻿using AutoMapper;
using BSA.BL.DTO.Task;
using BSA.BL.Exceptions;
using BSA.Repo.Enums;
using BSA.Repo.Models;
using BSA.Repo.UnitOfWorker;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BL.Services
{
    public class TaskService
    {
        private readonly UnitOfWorker _uow;
        private readonly IMapper _mapper;

        public TaskService(UnitOfWorker uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public IEnumerable<TaskDTO> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_uow.Tasks.GetAll());
        }

        public TaskDTO GetTaskById(int id)
        {
            var task = _uow.Tasks.GetById(id);
            if(task == null)
            {
                throw new NotFoundException(nameof(Task), id);
            }

            return _mapper.Map<TaskDTO>(task);
        }

        public TaskDTO AddTask(NewTaskDTO task)
        {
            var taskEntity = _mapper.Map<Task>(task);

            var proj = _uow.Projects.GetById(task.ProjectId);
            if (proj == null) throw new NotFoundException(nameof(Project), task.ProjectId);
            var user = _uow.Users.GetById(task.PerformerId);
            if(user == null) throw new NotFoundException(nameof(User), task.ProjectId);

            _uow.Tasks.Add(taskEntity);
            _uow.Save();

            var newTask = _uow.Tasks.GetById(taskEntity.Id);
            return _mapper.Map<TaskDTO>(newTask);
        }

        public void UpdateTask(NewTaskDTO task, int id)
        {
            var taskCheck = _uow.Tasks.GetById(id);
            if (taskCheck == null) throw new NotFoundException(nameof(Task), id);
            var proj = _uow.Projects.GetById(task.ProjectId);
            if (proj == null) throw new NotFoundException(nameof(Project), task.ProjectId);
            var user = _uow.Users.GetById(task.PerformerId);
            if (user == null) throw new NotFoundException(nameof(User), task.ProjectId);

            var taskEntity = _mapper.Map<Task>(task);
            taskEntity.Id = id;
            taskEntity.CreatedAt = taskCheck.CreatedAt;

            _uow.Tasks.Update(taskEntity);
            _uow.Save();
        }

        public void DeleteTask(int id)
        {
            var task = _uow.Tasks.GetById(id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Task), id);
            }

            _uow.Tasks.Remove(id);
            _uow.Save();
        }
    }
}
