﻿using AutoMapper;
using BSA.BL.DTO.Project;
using BSA.BL.DTO.Task;
using BSA.BL.DTO.User;
using BSA.BL.Exceptions;
using BSA.Repo.Enums;
using BSA.Repo.Models;
using BSA.Repo.UnitOfWorker;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BL.Services
{
    public class UserService
    {
        private readonly UnitOfWorker _uow;
        private readonly IMapper _mapper;

        public UserService(UnitOfWorker uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_uow.Users.GetAll());
        }

        public UserDTO GetUserById(int id)
        {
            var user = _uow.Users.GetById(id);
            if(user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }

            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO AddUser(NewUserDTO user)
        {
            var userEntity = _mapper.Map<User>(user);

            if (user.TeamId != null)
            {
                var team = _uow.Teams.GetById((int)user.TeamId);
                if (team == null) throw new NotFoundException(nameof(Team), (int)user.TeamId);
            }
            
            _uow.Users.Add(userEntity);
            _uow.Save();

            var newUser = _uow.Users.GetById(userEntity.Id);
            return _mapper.Map<UserDTO>(newUser);
        }

        public void UpdateUser(NewUserDTO user, int id)
        {
            var userCheck = _uow.Users.GetById(id);
            if (userCheck == null) throw new NotFoundException(nameof(User), id);
            if (user.TeamId != null)
            {
                var team = _uow.Teams.GetById((int)user.TeamId);
                if (team == null) throw new NotFoundException(nameof(Team), (int)user.TeamId);
            }

            var userEntity = _mapper.Map<User>(user);
            userEntity.Id = id;
            userEntity.RegisteredAt = userCheck.RegisteredAt;

            _uow.Users.Update(userEntity);
            _uow.Save();
        }

        public void DeleteUser(int id)
        {
            var user = _uow.Users.GetById(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }

            _uow.Users.Remove(id);
            _uow.Save();
        }

        // 6
        public UserMainInfoDTO GetMainInformationAboutUser(int userId)
        {
            var projects = _uow.Projects.GetAll();
            var user = _uow.Users.GetById(userId);
            var tasks = _uow.Tasks.GetAll();

            return new UserMainInfoDTO
            {
                User = _mapper.Map<UserDTO>(user),
                LastProject = _mapper.Map<ProjectDTO>(projects.Where(p => p.AuthorId == userId).Any() ?
                                                      projects.Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).First() : null),
                NumberOfTasksOfLastProject = projects.Where(p => p.AuthorId == userId).Any() ?
                                             projects.Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).First().Tasks.Count() : 0,
                NumberOfNotFinishedTasks = tasks.Count(t => t.PerformerId == userId && t.State != TaskState.Finished),
                LongestTask = _mapper.Map<TaskDTO>(tasks.Where(t => t.PerformerId == userId)
                                               .Aggregate((max, t) => (t.FinishedAt - t.CreatedAt > max.FinishedAt - max.CreatedAt ? t : max)))
            };
        }

        // 5
        public IEnumerable<UserWithTasksDTO> GetSortedUsersWithSortedTasks()
        {
            var users = _uow.Users.GetAll();
            var tasks = _uow.Tasks.GetAll();

            return users.OrderBy(u => u.FirstName)
                .GroupJoin(tasks, u => u.Id, t => t.PerformerId, (u, t) => new UserWithTasksDTO
                {
                    User = _mapper.Map<UserDTO>(u),
                    Tasks = _mapper.Map<IEnumerable<TaskDTO>>(t.OrderByDescending(t => t.Name.Length).ToList())
                }).ToList();
        }

        // 1
        public IDictionary<int, int> GetCountOfTasksOfUserInProject(int userId)
        {
            var projects = _uow.Projects.GetAll();

            return projects.ToDictionary(p => p.Id, t => t.Tasks.Count(ts => ts.PerformerId == userId));
        }

        // 2
        public IEnumerable<TaskDTO> GetTasksOfUserNameLengthLessFourtyFive(int userId)
        {
            var tasks = _uow.Tasks.GetAll();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45).ToList());
        }

        // 3
        public IEnumerable<ShortTaskInfoDTO> GetTasksOfUserFinishedThisYear(int userId)
        {
            var tasks = _uow.Tasks.GetAll();

            return tasks.Where(t => t.PerformerId == userId && t.State == TaskState.Finished && t.FinishedAt.Year == 2020)
                        .Select(t => new ShortTaskInfoDTO() { Id = t.Id, Name = t.Name }).ToList();
        }
    }
}
