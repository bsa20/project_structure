﻿using AutoMapper;
using BSA.BL.DTO.Team;
using BSA.Repo.Models;

namespace BSA.BL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<NewTeamDTO, Team>();
        }
    }
}
