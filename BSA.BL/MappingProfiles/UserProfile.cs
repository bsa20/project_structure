﻿using AutoMapper;
using BSA.BL.DTO.User;
using BSA.Repo.Models;

namespace BSA.BL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<NewUserDTO, User>();
        }
    }
}
