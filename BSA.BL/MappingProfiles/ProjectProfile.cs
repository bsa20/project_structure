﻿using AutoMapper;
using BSA.BL.DTO.Project;
using BSA.Repo.Models;

namespace BSA.BL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<NewProjectDTO, Project>();
        }
    }
}
