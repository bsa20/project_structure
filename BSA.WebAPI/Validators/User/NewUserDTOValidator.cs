﻿using BSA.BL.DTO.User;
using FluentValidation;

namespace BSA.WebAPI.Validators.User
{
    public class NewUserDTOValidator : AbstractValidator<NewUserDTO>
    {
        public NewUserDTOValidator()
        {
            RuleFor(u => u.Birthday).NotNull().Must(time => time.Ticks > 0).WithMessage("Birthday is necessary");
            RuleFor(u => u.Email).EmailAddress();
            RuleFor(u => u.TeamId).Must(id => id > 0).WithMessage("TeamId should be bigger than 0").When(id => id != null);
        }
    }
}
