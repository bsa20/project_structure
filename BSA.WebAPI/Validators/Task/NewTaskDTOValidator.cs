﻿using BSA.BL.DTO.Task;
using FluentValidation;
using System;

namespace BSA.WebAPI.Validators.Task
{
    public class NewTaskDTOValidator : AbstractValidator<NewTaskDTO>
    {
        public NewTaskDTOValidator()
        {
            RuleFor(t => t.FinishedAt).NotNull().Must(time => time.Ticks > DateTime.Now.Ticks).WithMessage("Field finishedAt should be later than now");
            RuleFor(t => t.PerformerId).NotNull().Must(id => id > 0).WithMessage("PerformerId should be bigger than 0");
            RuleFor(t => t.ProjectId).NotNull().Must(id => id > 0).WithMessage("ProjectId should be bigger than 0");
            RuleFor(t => t.State).NotNull().IsInEnum();
        }
    }
}
