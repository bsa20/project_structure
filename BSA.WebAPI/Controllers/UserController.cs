﻿using BSA.BL.DTO.Task;
using BSA.BL.DTO.User;
using BSA.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetAll()
        {
            return Ok(_userService.GetAllUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            return Ok(_userService.GetUserById(id));
        }

        [HttpPost]
        public ActionResult<UserDTO> Post([FromBody] NewUserDTO user)
        {
            return Ok(_userService.AddUser(user));
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromBody] NewUserDTO user, int id)
        {
            _userService.UpdateUser(user, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _userService.DeleteUser(id);
            return StatusCode(204);
        }

        [HttpGet("{userId}/maininfo")]
        public ActionResult<UserMainInfoDTO> GetMainInfoAboutUser(int userId)
        {
            return Ok(_userService.GetMainInformationAboutUser(userId));
        }

        [HttpGet("sorted/tasks/sorted")]
        public ActionResult<IEnumerable<UserWithTasksDTO>> GetSortedUsersWithSortedTasks()
        {
            return Ok(_userService.GetSortedUsersWithSortedTasks());
        }

        [HttpGet("{userId}/tasksInProjects/count")]
        public ActionResult<IDictionary<int, int>> GetCountOfTasksOfUserInProject(int userId)
        {
            return Ok(_userService.GetCountOfTasksOfUserInProject(userId));
        }

        [HttpGet("{userId}/tasks")]
        public ActionResult<IEnumerable<TaskDTO>> GetTasksOfUser(int userId)
        {
            return Ok(_userService.GetTasksOfUserNameLengthLessFourtyFive(userId));
        }


        [HttpGet("{userId}/thisYear")]
        public ActionResult<IEnumerable<ShortTaskInfoDTO>> GetTasksOfUserFinishedThisYear(int userId)
        {
            return Ok(_userService.GetTasksOfUserFinishedThisYear(userId));
        }
    }
}