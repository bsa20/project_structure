﻿using BSA.BL.DTO.Project;
using BSA.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectService _projService;

        public ProjectController(ProjectService projService)
        {
            _projService = projService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetAll()
        {
            return Ok(_projService.GetAllProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return Ok(_projService.GetProjectById(id));
        }

        [HttpPost]
        public ActionResult<ProjectDTO> Post([FromBody] NewProjectDTO project)
        {
            return Ok(_projService.AddProject(project));
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromBody] NewProjectDTO project, int id)
        {
            _projService.UpdateProject(project, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _projService.DeleteProject(id);
            return StatusCode(204);
        }

        [HttpGet("info/main")]
        public ActionResult<IEnumerable<ProjectMainInfoDTO>> GetMainInfo()
        {
            return Ok(_projService.GetMainInfoAboutProjects());
        }
    }
}
