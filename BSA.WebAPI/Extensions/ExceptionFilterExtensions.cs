﻿using BSA.BL.Exceptions;
using System;
using System.Net;

namespace BSA.WebAPI.Extensions
{
    public static class ExceptionFilterExtensions
    {
        public static HttpStatusCode ParseException(this Exception exception)
        {
            switch (exception)
            {
                case NotFoundException _:
                    return HttpStatusCode.NotFound;
                case ArgumentException _:
                    return HttpStatusCode.BadRequest;
                default:
                    return HttpStatusCode.InternalServerError;
            }
        }
    }
}
