﻿using AutoMapper;
using BSA.BL.MappingProfiles;
using BSA.BL.Services;
using BSA.Repo.Context;
using BSA.Repo.Context.Abstract;
using BSA.Repo.UnitOfWorker;
using BSA.WebAPI.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace BSA.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
        }

        public static void RegisterUnitOfWorker(this IServiceCollection services)
        {
            services.AddScoped<IContext, BsaContext>();
            services.AddScoped<UnitOfWorker>();
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<ProjectService>();
            services.AddScoped<TaskService>();
            services.AddScoped<TeamService>();
            services.AddScoped<UserService>();
        }
    }
}
