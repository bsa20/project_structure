﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Client.Services.HttpServices.Abstract
{
    public abstract class HttpService<Tentity, NewEntity>
    {
        protected readonly HttpClient httpClient;
        protected const string URL = "https://localhost:44337/";

        public HttpService()
        {
            this.httpClient = new HttpClient();
            this.httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        protected async Task<IEnumerable<Tentity>> GetAllEntities(string sufix)
        {
            var httpResponse = await this.httpClient.GetAsync(URL + sufix);
            httpResponse.EnsureSuccessStatusCode();
            var jsonString = await httpResponse.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<IEnumerable<Tentity>>(jsonString);

            return result;
        }

        protected async Task<Tentity> GetEntityById(string sufix, int id)
        {
            var httpResponse = await this.httpClient.GetAsync(URL + sufix + "/" + id);
            httpResponse.EnsureSuccessStatusCode();
            var jsonString = await httpResponse.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<Tentity>(jsonString);

            return result;
        }

        protected async Task<Tentity> PostRequest(string sufix, NewEntity entity)
        {
            var json = JsonConvert.SerializeObject(entity);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await this.httpClient.PostAsync(URL + sufix, content);

            string result = await response.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<Tentity>(result);
            return obj;
        }

        protected async Task<string> PutRequest(string sufix, int id, NewEntity entity)
        {
            var json = JsonConvert.SerializeObject(entity);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await this.httpClient.PutAsync(URL + sufix + "/" + id, content);

            string result = await response.Content.ReadAsStringAsync();
            return result;
        }

        protected async Task<string> DeleteRequest(string sufix, int id)
        {
            var response = await this.httpClient.DeleteAsync(URL + sufix + "/" + id);

            string result = await response.Content.ReadAsStringAsync();
            return result;
        }

        protected async Task<String> GetRequest(string sufix)
        {
            var response = await this.httpClient.GetAsync(URL + sufix);

            var jsonString = await response.Content.ReadAsStringAsync();
            return jsonString;
        }
    }
}
