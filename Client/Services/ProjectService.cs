﻿using Client.API;
using Client.DTO.Project;
using Client.Services.HttpServices.Abstract;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Services
{
    public class ProjectService : HttpService<ProjectDTO, NewProjectDTO>
    {
        private readonly ApiOptions _apiOptions;

        public ProjectService()
        {
            _apiOptions = new ApiOptions();
        }

        public async Task<IEnumerable<ProjectDTO>> GetAll()
        {
            return await this.GetAllEntities(_apiOptions.GetProjectSufix);
        }

        public async Task<ProjectDTO> GetById(int id)
        {
            return await this.GetEntityById(_apiOptions.GetProjectSufix, id);
        }

        public async Task<ProjectDTO> Create(NewProjectDTO project)
        {
            return await this.PostRequest(_apiOptions.PostProjectSufix, project);
        }

        public async Task<string> Update(NewProjectDTO project, int id)
        {
            return await this.PutRequest(_apiOptions.PutProjectSufix, id, project);
        }

        public async Task<string> Delete(int id)
        {
            return await this.DeleteRequest(_apiOptions.DeleteProjectSufix, id);
        }

        public async Task<IEnumerable<ProjectMainInfoDTO>> GetProjectsMainInfo()
        {
            var json = await this.GetRequest(_apiOptions.GetProjectsMainInfo);
            return JsonConvert.DeserializeObject<IEnumerable<ProjectMainInfoDTO>>(json);
        }
    }
}
