﻿using Client.API;
using Client.DTO.Team;
using Client.Services.HttpServices.Abstract;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Services
{
    public class TeamService : HttpService<TeamDTO, NewTeamDTO>
    {
        private readonly ApiOptions _apiOptions;

        public TeamService()
        {
            _apiOptions = new ApiOptions();
        }

        public async Task<IEnumerable<TeamDTO>> GetAll()
        {
            return await this.GetAllEntities(_apiOptions.GetTeamSufix);
        }

        public async Task<TeamDTO> GetById(int id)
        {
            return await this.GetEntityById(_apiOptions.GetTeamSufix, id);
        }

        public async Task<TeamDTO> Create(NewTeamDTO team)
        {
            return await this.PostRequest(_apiOptions.PostTeamSufix, team);
        }

        public async Task<string> Update(NewTeamDTO team, int id)
        {
            return await this.PutRequest(_apiOptions.PutTeamSufix, id, team);
        }

        public async Task<string> Delete(int id)
        {
            return await this.DeleteRequest(_apiOptions.DeleteTeamSufix, id);
        }

        public async Task<IEnumerable<TeamWithUsersDTO>> GetTeamWithUsersOlderThanTen()
        {
            var json = await this.GetRequest(_apiOptions.GetTeamWithUsersOlderThanTen);
            return JsonConvert.DeserializeObject<IEnumerable<TeamWithUsersDTO>>(json);
        }
    }
}
