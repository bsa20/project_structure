﻿using Client.API;
using Client.DTO.Task;
using Client.DTO.User;
using Client.Services.HttpServices.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Services
{
    public class UserService : HttpService<UserDTO, NewUserDTO>
    {
        private readonly ApiOptions _apiOptions;

        public UserService()
        {
            _apiOptions = new ApiOptions();
        }

        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            return await this.GetAllEntities(_apiOptions.GetUserSufix);
        }

        public async Task<UserDTO> GetById(int id)
        {
            return await this.GetEntityById(_apiOptions.GetUserSufix, id);
        }

        public async Task<UserDTO> Create(NewUserDTO user)
        {
            return await this.PostRequest(_apiOptions.PostUserSufix, user);
        }

        public async Task<string> Update(NewUserDTO user, int id)
        {
            return await this.PutRequest(_apiOptions.PutUserSufix, id, user);
        }

        public async Task<string> Delete(int id)
        {
            return await this.DeleteRequest(_apiOptions.DeleteUserSufix, id);
        }

        public async Task<IEnumerable<ShortTaskInfoDTO>> GetTasksOfUserFinishedThisYear(int userId)
        {
            _apiOptions.UserId = userId;
            var json = await this.GetRequest(_apiOptions.GetTasksOfUserFinishedThisYear);

            return JsonConvert.DeserializeObject<IEnumerable<ShortTaskInfoDTO>>(json);
        }

        public async Task<UserMainInfoDTO> GetMainInfoAboutUser(int userId)
        {
            _apiOptions.UserId = userId;
            var json = await this.GetRequest(_apiOptions.GetMainInfoAboutUser);

            return JsonConvert.DeserializeObject<UserMainInfoDTO>(json);
        }

        public async Task<IEnumerable<UserWithTasksDTO>> GetSortedUsersWithSortedTasks()
        {
            var json = await this.GetRequest(_apiOptions.GetSortedUsersWithSortedTasks);

            return JsonConvert.DeserializeObject<IEnumerable<UserWithTasksDTO>>(json);
        }

        public async Task<IDictionary<int, int>> GetCountOfTasksOfUserInProject(int userId)
        {
            _apiOptions.UserId = userId;
            var json = await this.GetRequest(_apiOptions.GetCountOfTasksOfUserInProject);

            return JsonConvert.DeserializeObject<IDictionary<int, int>>(json);
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksOfUser(int userId)
        {
            _apiOptions.UserId = userId;
            var json = await this.GetRequest(_apiOptions.GetTasksOfUser);

            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(json);
        }
    }
}
