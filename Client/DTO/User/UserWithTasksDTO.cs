﻿using Client.DTO.Task;
using System.Collections.Generic;

namespace Client.DTO.User
{
    public class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }
    }
}
