﻿using Client.DTO.User;
using System.Collections.Generic;

namespace Client.DTO.Team
{
    public class TeamWithUsersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Users { get; set; }
    }
}
