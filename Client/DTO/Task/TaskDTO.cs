﻿using Client.DTO.Project;
using Client.DTO.User;
using System;

namespace Client.DTO.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public Enums.TaskState State { get; set; }

        public int ProjectId { get; set; }
        public ProjectDTO Project { get; set; }

        public int PerformerId { get; set; }
        public UserDTO Performer { get; set; }
    }
}
