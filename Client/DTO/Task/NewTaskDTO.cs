﻿using System;

namespace Client.DTO.Task
{
    public class NewTaskDTO
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime FinishedAt { get; set; }
        public Enums.TaskState State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
