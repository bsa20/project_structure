﻿using System;

namespace Client.DTO.Project
{
    public class NewProjectDTO
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }

        public int TeamId { get; set; }
    }
}
