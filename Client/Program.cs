﻿using Client.DTO.Project;
using Client.DTO.Task;
using Client.DTO.Team;
using Client.DTO.User;
using Client.Services;
using System;
using System.Linq;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var projService = new ProjectService();
            var taskService = new TaskService();
            var teamService = new TeamService();
            var userService = new UserService();

            while (true)
            {
                try
                {
                    menu(projService, taskService, teamService, userService);
                    break;
                }
                catch
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error");
                    Console.ResetColor();
                }
            }
        }

        private static void menu(ProjectService projeService,
                                 TaskService taskService,
                                 TeamService teamService,
                                 UserService userService)
        {
            while (true)
            {
                Console.WriteLine("1 : Create\n" +
                    "2  : Read\n" +
                    "3  : Update\n" +
                    "4  : Delete\n" +
                    "5  : Read by id\n" +
                    "6  : Get main info abaout all projects (task 7)\n" +
                    "7  : Get tasks of user finished in 2020 (task 3)\n" +
                    "8  : Get teams with users older than 10 (task 4)\n" +
                    "9  : Get main info about user (task 6)\n" +
                    "10 : Get users order alphabeticaly with their tasks order by name length (task 5)\n" +
                    "11 : Get number of tasks of user in project (task 1)\n" +
                    "12 : Get tasks of user with name length <45 (task 2)\n" +
                    "0  : Exit\n");

                int x = enterNumber();
                if (x == -1) continue;

                switch (x)
                {
                    case 1:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        var entity = enterNewProject();

                                        var result = projeService.Create(entity).GetAwaiter().GetResult();
                                        showProject(result);
                                        break;
                                    }
                                case 2:
                                    {
                                        var entity = enterNewTask();

                                        var result = taskService.Create(entity).GetAwaiter().GetResult();
                                        showTask(result);
                                        break;
                                    }
                                case 3:
                                    {
                                        var entity = enterNewTeam();

                                        var result = teamService.Create(entity).GetAwaiter().GetResult();
                                        showTeam(result);
                                        break;
                                    }
                                case 4:
                                    {
                                        var entity = enterUser();

                                        var result = userService.Create(entity).GetAwaiter().GetResult();
                                        showUser(result);
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 2:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        var result = projeService.GetAll().GetAwaiter().GetResult();
                                        foreach (var item in result)
                                        {
                                            showProject(item);
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        var result = taskService.GetAll().GetAwaiter().GetResult();
                                        foreach (var item in result)
                                        {
                                            showTask(item);
                                        }
                                        break;
                                    }
                                case 3:
                                    {
                                        var result = teamService.GetAll().GetAwaiter().GetResult();
                                        foreach (var item in result)
                                        {
                                            showTeam(item);
                                        }
                                        break;
                                    }
                                case 4:
                                    {
                                        var result = userService.GetAll().GetAwaiter().GetResult();
                                        foreach (var item in result)
                                        {
                                            showUser(item);
                                        }
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 3:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var entity = enterNewProject();

                                        var result = projeService.Update(entity, id).GetAwaiter().GetResult();
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var entity = enterNewTask();

                                        var result = taskService.Update(entity, id).GetAwaiter().GetResult();
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var entity = enterNewTeam();

                                        var result = teamService.Update(entity, id).GetAwaiter().GetResult();
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var entity = enterUser();

                                        var result = userService.Update(entity, id).GetAwaiter().GetResult();
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 4:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var result = projeService.Delete(id).GetAwaiter().GetResult();
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var result = taskService.Delete(id).GetAwaiter().GetResult();
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var result = teamService.Delete(id).GetAwaiter().GetResult();
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var result = userService.Delete(id).GetAwaiter().GetResult();
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 5:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;
                                        ProjectDTO result = null;
                                        try
                                        {
                                            result = projeService.GetById(id).GetAwaiter().GetResult();
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                        if(result != null) showProject(result);
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;
                                        TaskDTO result = null;
                                        try
                                        {
                                            result = taskService.GetById(id).GetAwaiter().GetResult();
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                        if (result != null) showTask(result);
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;
                                        TeamDTO result = null;
                                        try
                                        {
                                            result = teamService.GetById(id).GetAwaiter().GetResult();
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                        if (result != null) showTeam(result);
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;
                                        UserDTO result = null;
                                        try
                                        {
                                            result = userService.GetById(id).GetAwaiter().GetResult();
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                        if (result != null) showUser(result);
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 6:
                        {
                            var result = projeService.GetProjectsMainInfo().GetAwaiter().GetResult();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Project id: {item.Project.Id}, Name: {item.Project.Name}");
                                if (item.LongestTaskByDescription != null)
                                {
                                    Console.WriteLine($"Longest task of project by description: id({item.LongestTaskByDescription.Id})\n" +
                                                        $"Shortest task of project by name: id({item.ShortetTaskByName.Id})");
                                }
                                Console.WriteLine($"Number of users in team of project(description > 20 and count of tasks < 3): {item.NumberOfUsersInTeam}\n");
                            }
                            break;
                        }
                    case 7:
                        {
                            Console.WriteLine("Enter id of user");
                            var id = enterNumber();
                            if (id == -1) continue;

                            var result = userService.GetTasksOfUserFinishedThisYear(id).GetAwaiter().GetResult();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Task id: {item.Id}, Name: {item.Name}");
                            }
                            if (result.Count() == 0) Console.WriteLine("None");

                            break;
                        }
                    case 8:
                        {
                            var result = teamService.GetTeamWithUsersOlderThanTen().GetAwaiter().GetResult();
                            foreach (var team in result)
                            {
                                Console.WriteLine($"Team id: {team.Id}, Team: {team.Name}");
                                foreach (var user in team.Users)
                                {
                                    Console.WriteLine($"\t{user.FirstName} {user.LastName}, registered at: {user.RegisteredAt}");
                                }
                            }
                            break;
                        }
                    case 9:
                        {
                            Console.WriteLine("Enter id of user");
                            var id = enterNumber();
                            if (id == -1) continue;
                            var result = userService.GetMainInfoAboutUser(id).GetAwaiter().GetResult();

                            Console.WriteLine($"{result.User.FirstName} {result.User.LastName}");
                            if (result.LastProject != null) {
                                Console.WriteLine($"Last project: id ({result.LastProject.Id}) name ({result.LastProject.Name})\n" +
                                                  $"Number of taks on last project: {result.NumberOfTasksOfLastProject}");
                            }
                            Console.WriteLine($"Number of not finished tasks: {result.NumberOfNotFinishedTasks}\n" +
                                              $"Longest task: id ({result.LongestTask.Id}) name ({result.LongestTask.Name})\n");
                            break;
                        }
                    case 10:
                        {
                            var result = userService.GetSortedUsersWithSortedTasks().GetAwaiter().GetResult();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"{item.User.FirstName} {item.User.LastName}");
                                foreach (var task in item.Tasks)
                                {
                                    Console.WriteLine($"\tTask: id({task.Id}) {task.Name}");
                                }
                            }
                            break;
                        }
                    case 11:
                        {
                            Console.WriteLine("Enter id of user");
                            var id = enterNumber();
                            if (id == -1) continue;

                            var result = userService.GetCountOfTasksOfUserInProject(id).GetAwaiter().GetResult();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Project id: {item.Key}, count of tasks: {item.Value}");
                            }
                            break;
                        }
                    case 12:
                        {
                            Console.WriteLine("Enter id of user");
                            var id = enterNumber();
                            if (id == -1) continue;

                            var result = userService.GetTasksOfUser(id).GetAwaiter().GetResult();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Task id: {item.Id}, Project id: {item.ProjectId}, Name: {item.Name}");
                            }
                            if (result.Count() == 0) Console.WriteLine("None");
                            break;
                        }
                    case 0:
                        {
                            return;
                        }
                }
                Console.WriteLine("\nPress any key to return in menu");
                Console.ReadKey();
                Console.Clear();
            }
        }

        private static int enterNumber()
        {
            int x;
            try { x = int.Parse(Console.ReadLine().Trim()); }
            catch
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Enter number");
                Console.ResetColor();
                return -1;
            }
            return x;
        }

        private static int selectEntity()
        {
            Console.WriteLine("\t1 : Project\n" +
                                "\t2 : Task\n" +
                                "\t3 : Team\n" +
                                "\t4 : User\n" +
                                "\t0 : Exit");
            int x;
            try { x = int.Parse(Console.ReadLine().Trim()); }
            catch
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Enter number");
                Console.ResetColor();
                return -1;
            }
            return x;
        }

        private static void showTeam(TeamDTO team)
        {
            Console.WriteLine();
            Console.WriteLine($"id: {team.Id}\n" +
                $"name: {team.Name}\n");
            Console.WriteLine();
        }

        private static void showTask(TaskDTO task)
        {
            Console.WriteLine();
            Console.WriteLine($"id: {task.Id}\n" +
                $"name: {task.Name}\n" +
                $"performer id: {task.PerformerId}\n" +
                $"finished at: {task.FinishedAt}\n");
            Console.WriteLine();
        }

        private static void showProject(ProjectDTO proj)
        {
            Console.WriteLine();
            Console.WriteLine($"id: {proj.Id}\n" +
                $"name: {proj.Name}\n" +
                $"authorId: {proj.AuthorId}\n" +
                $"teamId: {proj.TeamId}\n" +
                $"deadline: {proj.Deadline}\n");
            Console.WriteLine();
        }

        private static void showUser(UserDTO user)
        {
            Console.WriteLine();
            Console.WriteLine($"id: {user.Id}\n" +
                $"first name: {user.FirstName}\n" +
                $"last name: {user.LastName}\n" +
                $"email: {user.Email}" +
                $"birthday: {user.Birthday}\n");
            Console.WriteLine();
        }

        private static NewUserDTO enterUser()
        {
            Console.Write("First Name: ");
            var fName = Console.ReadLine();
            Console.Write("LastName Name: ");
            var lName = Console.ReadLine();
            Console.Write("Email Name: ");
            var email = Console.ReadLine();
            Console.WriteLine("Birthday: ");
            var birth = enterDate();
            Console.Write("TeamId: ");
            var teamId = int.Parse(Console.ReadLine().Trim());

            var entity = new NewUserDTO
            {
                FirstName = fName,
                LastName = lName,
                Email = email,
                Birthday = birth,
                TeamId = teamId
            };

            return entity;
        }

        private static NewTeamDTO enterNewTeam()
        {
            Console.Write("Name: ");
            var name = Console.ReadLine();

            var entity = new NewTeamDTO
            {
                Name = name
            };
            return entity;
        }

        private static NewTaskDTO enterNewTask()
        {
            Console.Write("Name: ");
            var name = Console.ReadLine();
            Console.Write("Description: ");
            var desc = Console.ReadLine();
            Console.WriteLine("FinishedAt: ");
            var deadline = enterDate();
            Console.Write("PerforemrId: ");
            var perforemerId = int.Parse(Console.ReadLine().Trim());
            Console.Write("ProjectId: ");
            var projId = int.Parse(Console.ReadLine().Trim());
            Console.Write("State(0 - 3)");
            var state = int.Parse(Console.ReadLine().Trim());

            var entity = new NewTaskDTO()
            {
                Name = name,
                Description = desc,
                FinishedAt = deadline,
                PerformerId = perforemerId,
                ProjectId = projId,
                State = (Enums.TaskState)state
            };
            return entity;
        }

        private static NewProjectDTO enterNewProject()
        {
            Console.Write("Name: ");
            var name = Console.ReadLine();
            Console.Write("Description: ");
            var desc = Console.ReadLine();
            Console.WriteLine("Deadlene: ");
            var deadline = enterDate();
            Console.Write("AuthorId: ");
            var authorId = int.Parse(Console.ReadLine().Trim());
            Console.Write("TeamId: ");
            var teamId = int.Parse(Console.ReadLine().Trim());

            var entity = new NewProjectDTO()
            {
                Name = name,
                Description = desc,
                AuthorId = authorId,
                TeamId = teamId,
                Deadline = deadline
            };
            return entity;
        }

        private static DateTime enterDate()
        {
            Console.Write("Enter a day: ");
            int day = int.Parse(Console.ReadLine().Trim());
            Console.Write("Enter a month: ");
            int month = int.Parse(Console.ReadLine().Trim());
            Console.Write("Enter a year: ");
            int year = int.Parse(Console.ReadLine().Trim());

            return new DateTime(year, month, day);
        }
    }
}
