﻿using BSA.Repo.Context.Abstract;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.Repo.Repository
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly IContext _ctx;

        public TaskRepository(IContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(Task entity)
        {
            entity.Id = _ctx.Tasks.Max(t => t.Id) + 1;
            entity.CreatedAt = DateTime.Now;
            _ctx.Tasks = _ctx.Tasks.Append(entity).ToList();
        }

        public IEnumerable<Task> GetAll()
        {
            return _ctx.Tasks.ToList();
        }

        public Task GetById(int id)
        {
            return _ctx.Tasks.FirstOrDefault(p => p.Id == id);
        }

        public void Remove(int id)
        {
            _ctx.Tasks = _ctx.Tasks.Where(p => p.Id != id).ToList();
        }

        public void Update(Task entity)
        {
            _ctx.Tasks = _ctx.Tasks.Select(p =>
            {
                if (p.Id == entity.Id) return entity;
                else return p;
            }).ToList();
        }
    }
}
