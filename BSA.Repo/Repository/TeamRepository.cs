﻿using BSA.Repo.Context.Abstract;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.Repo.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly IContext _ctx;

        public TeamRepository(IContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(Team entity)
        {
            entity.Id = _ctx.Teams.Max(t => t.Id) + 1;
            entity.CreatedAt = DateTime.Now;
            _ctx.Teams = _ctx.Teams.Append(entity).ToList();
        }

        public IEnumerable<Team> GetAll()
        {
            return _ctx.Teams.ToList();
        }

        public Team GetById(int id)
        {
            return _ctx.Teams.FirstOrDefault(p => p.Id == id);
        }

        public void Remove(int id)
        {
            _ctx.Teams = _ctx.Teams.Where(p => p.Id != id).ToList();
            _ctx.Users = _ctx.Users.Where(u => u.TeamId != id)
                                   .Union(_ctx.Users.Where(u => u.TeamId == id).Select(u =>
                                   {
                                       u.Team = null;
                                       u.TeamId = null;
                                       return u;
                                   }));
        }

        public void Update(Team entity)
        {
            _ctx.Teams = _ctx.Teams.Select(p =>
            {
                if (p.Id == entity.Id) return entity;
                else return p;
            }).ToList();
        }
    }
}
