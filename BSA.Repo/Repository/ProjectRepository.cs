﻿using BSA.Repo.Context.Abstract;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.Repo.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly IContext _ctx;

        public ProjectRepository(IContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(Project entity)
        {
            entity.Id = _ctx.Projects.Max(p => p.Id) + 1;
            entity.CreatedAt = DateTime.Now;
            _ctx.Projects = _ctx.Projects.Append(entity).ToList();
        }

        public IEnumerable<Project> GetAll()
        {
            return _ctx.Projects.ToList();
        }

        public Project GetById(int id)
        {
            var proj = _ctx.Projects.FirstOrDefault(p => p.Id == id);

            return proj;
        }

        public void Remove(int id)
        {
            _ctx.Projects = _ctx.Projects.Where(p => p.Id != id).ToList();
            _ctx.Tasks = _ctx.Tasks.Where(t => t.ProjectId != id).ToList();
        }

        public void Update(Project entity)
        {
            _ctx.Projects = _ctx.Projects.Select(p =>
            {
                if (p.Id == entity.Id) return entity;
                else return p;
            }).ToList();
        }
    }
}
