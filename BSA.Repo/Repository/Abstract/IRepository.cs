﻿using System.Collections.Generic;

namespace BSA.Repo.Repository.Abstract
{
    public interface IRepository<TEntity>
    {
        public abstract void Add(TEntity entity);
        public abstract void Remove(int id);
        public abstract void Update(TEntity entity);
        public abstract TEntity GetById(int id);
        public abstract IEnumerable<TEntity> GetAll();
    }
}
