﻿using BSA.Repo.Context.Abstract;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.Repo.Repository
{
    public class UserRepository : IRepository<User>
    {
        private readonly IContext _ctx;

        public UserRepository(IContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(User entity)
        {
            entity.Id = _ctx.Users.Max(u => u.Id) + 1;
            entity.RegisteredAt = DateTime.Now;
            _ctx.Users = _ctx.Users.Append(entity).ToList();
        }

        public IEnumerable<User> GetAll()
        {
            return _ctx.Users.ToList();
        }

        public User GetById(int id)
        {
            return _ctx.Users.FirstOrDefault(p => p.Id == id);
        }

        public void Remove(int id)
        {
            _ctx.Users = _ctx.Users.Where(u => u.Id != id).ToList();
            var ids = new List<int>();
            _ctx.Projects = _ctx.Projects.Where(p => { if (p.AuthorId == id) ids.Add(p.Id); return p.AuthorId != id; }).ToList();
            _ctx.Tasks = _ctx.Tasks.Where(t => !ids.Contains(t.ProjectId)).Where(t => t.PerformerId != id).ToList();
        }

        public void Update(User entity)
        {
            _ctx.Users = _ctx.Users.Select(p =>
            {
                if (p.Id == entity.Id) return entity;
                else return p;
            }).ToList();
        }
    }
}
