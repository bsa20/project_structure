﻿using BSA.Repo.Context.Abstract;
using BSA.Repo.Repository;

namespace BSA.Repo.UnitOfWorker
{
    public class UnitOfWorker
    {
        private readonly IContext _ctx;
        private ProjectRepository projectRepository;
        private TaskRepository taskRepository;
        private TeamRepository teamRepository;
        private UserRepository userRepository;

        public UnitOfWorker(IContext ctx)
        {
            _ctx = ctx;
        }

        public ProjectRepository Projects
        {
            get
            {
                if (projectRepository == null) return new ProjectRepository(_ctx);
                return projectRepository;
            }
        }

        public TaskRepository Tasks
        {
            get
            {
                if (taskRepository == null) return new TaskRepository(_ctx);
                return taskRepository;
            }
        }

        public TeamRepository Teams
        {
            get
            {
                if (teamRepository == null) return new TeamRepository(_ctx);
                return teamRepository;
            }
        }

        public UserRepository Users
        {
            get
            {
                if (userRepository == null) return new UserRepository(_ctx);
                return userRepository;
            }
        }

        public void Save()
        {
            _ctx.SaveChanges();
        }
    }
}
