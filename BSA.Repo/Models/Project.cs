﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA.Repo.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        [JsonIgnore]
        public User Author { get; set; }

        public int TeamId { get; set; }
        [JsonIgnore]
        public Team Team { get; set; } 
        
        [JsonIgnore]
        public IEnumerable<Task> Tasks { get; set; }
    }
}
