﻿using BSA.Repo.Enums;
using Newtonsoft.Json;
using System;

namespace BSA.Repo.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }

        public int ProjectId { get; set; }
        [JsonIgnore]
        public Project Project { get; set; }

        public int PerformerId { get; set; }
        [JsonIgnore]
        public User Performer { get; set; }
    }
}
