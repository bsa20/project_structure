﻿using BSA.Repo.Context.Abstract;
using BSA.Repo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA.Repo.Context
{
    public class BsaContext : IContext
    {
        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Team> Teams { get; set; }
        public IEnumerable<Models.Task> Tasks { get; set; }

        public BsaContext()
        {
            getDataFromAPI();
        }

        private void getDataFromAPI()
        {
            this.Projects = JsonConvert.DeserializeObject<IEnumerable<Project>>(File.ReadAllText("..\\BSA.Repo\\data\\projects.json"));
            this.Users = JsonConvert.DeserializeObject<IEnumerable<User>>(File.ReadAllText("..\\BSA.Repo\\data\\users.json"));
            this.Teams = JsonConvert.DeserializeObject<IEnumerable<Team>>(File.ReadAllText("..\\BSA.Repo\\data\\teams.json"));
            this.Tasks = JsonConvert.DeserializeObject<IEnumerable<Models.Task>>(File.ReadAllText("..\\BSA.Repo\\data\\tasks.json"));

            configureDependencies();
        }

        private void configureDependencies()
        {
            this.Users = (from user in Users
                          join team in Teams
                          on user.TeamId equals team.Id
                          select loadUser(user, team)).Union(
                          from user in Users
                          where user.TeamId == null
                          select user
                          ).ToList();

            this.Tasks = (from task in Tasks
                          join project in Projects
                          on task.ProjectId equals project.Id
                          join user in Users
                          on task.PerformerId equals user.Id
                          select loadTask(task, project, user)).ToList();

            this.Projects = (from project in Projects
                             join author in Users
                             on project.AuthorId equals author.Id
                             join task in Tasks
                             on project.Id equals task.ProjectId
                             into tasksOfProject
                             join team in Teams
                             on project.TeamId equals team.Id
                             select loadProject(project, tasksOfProject.ToList(), author, team)).ToList();
        }

        private Project loadProject(Project project, IEnumerable<Models.Task> tasks, User author, Team team)
        {
            project.Tasks = tasks;
            project.Author = author;
            project.Team = team;
            return project;
        }

        private Models.Task loadTask(Models.Task task, Project project, User performer)
        {
            task.Project = project;
            task.Performer = performer;
            return task;
        }

        private User loadUser(User user, Team team)
        {
            user.Team = team;
            return user;
        }

        public void SaveChanges()
        {
            this.Teams = this.Teams.ToList();
            this.Projects = this.Projects.ToList();
            this.Tasks = this.Tasks.ToList();
            this.Users = this.Users.ToList();
            this.configureDependencies();

            File.WriteAllText("..\\BSA.Repo\\data\\projects.json", JsonConvert.SerializeObject(this.Projects));
            File.WriteAllText("..\\BSA.Repo\\data\\users.json", JsonConvert.SerializeObject(this.Users));
            File.WriteAllText("..\\BSA.Repo\\data\\teams.json", JsonConvert.SerializeObject(this.Teams));
            File.WriteAllText("..\\BSA.Repo\\data\\tasks.json", JsonConvert.SerializeObject(this.Tasks));
        }
    }
}
