﻿using BSA.Repo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA.Repo.Context.Abstract
{
    public interface IContext
    {
        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Team> Teams { get; set; }
        public IEnumerable<Models.Task> Tasks { get; set; }

        public void SaveChanges();
    }
}
